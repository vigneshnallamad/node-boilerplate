import test from 'ava';

test('getOrdersTestSuccess', t => {
    // TODO: Add test case
    // we can call the get API here and assert the response
    t.pass();
});

test('getOrdersTestFailure', t => {
    // TODO: Add test case
    // we can call the get API here and assert the response
    t.pass();
});

test('createOrdersTestSuccess', async t => {
    // TODO: Add test case
    // we can call the create API here and assert the response
    // we should mock sequelize model here
    const bar = Promise.resolve('bar');
    t.is(await bar, 'bar');
});

test('createOrdersTestFailure', async t => {
    // TODO: Add test case
    // we can call the create API here and assert the response
    // we should mock sequelize model here
    const bar = Promise.resolve('bar');
    t.is(await bar, 'bar');
});
