import { Model, DataTypes } from 'sequelize';
import sequelize from './_index';

export class Inventory extends Model {}

Inventory.init(
    {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: DataTypes.STRING,
            allowNull: false,
            field: 'name'
        },
        count: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'count'
        },
        remaining: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'remaining'
        }
    },
    {
        sequelize,
        modelName: 'inventory',
        tableName: 'inventory',
        timestamps: true,
        // Enable optimistic locking.  When enabled, sequelize will add a version count attribute
        // to the model and throw an OptimisticLockingError error when stale instances are saved.
        // This will prevent race condition while checking for remaining column
        version: true
    }
);
