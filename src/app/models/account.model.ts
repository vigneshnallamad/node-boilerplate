import { STRING, Model, TEXT, DataTypes } from 'sequelize';
import sequelize from './_index';

export class Account extends Model {}

Account.init(
    {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        name: {
            type: STRING,
            allowNull: false,
            field: 'name',
            // We can add more validation here
            validate: {
                notEmpty: {
                    msg: 'Name cannot be empty'
                }
            }
        },
        mobile: {
            type: DataTypes.STRING(20),
            allowNull: false,
            field: 'mobile',
            unique: true,
            validate: {
                notEmpty: {
                    msg: 'Mobile number cannot be empty'
                }
            }
        },
        address: {
            type: TEXT,
            allowNull: false,
            field: 'address',
            validate: {
                notEmpty: {
                    msg: 'Address cannot be empty'
                }
            }
        }
    },
    {
        sequelize,
        modelName: 'account',
        tableName: 'account',
        timestamps: true
    }
);
