import { Model, DataTypes } from 'sequelize';
import sequelize from './_index';
import { Account } from './account.model';
import { Inventory } from './inventory.model';
import { OrderInventory } from './orderInventory.model';

export class Orders extends Model {}

Orders.init(
    {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        accountId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'account_id',
            references: {
                model: Account,
                key: 'id'
            }
        }
    },
    {
        sequelize,
        modelName: 'Orders',
        tableName: 'orders',
        timestamps: true
    }
);

Orders.belongsTo(Account, {
    as: 'account',
    foreignKey: 'accountId',
    targetKey: 'id'
});

Account.hasMany(Orders, {
    as: 'orders',
    sourceKey: 'id',
    foreignKey: 'accountId'
});

Orders.belongsToMany(Inventory, {
    through: OrderInventory,
    as: 'inventory',
    foreignKey: 'orderId'
});

Inventory.belongsToMany(Orders, {
    through: OrderInventory,
    as: 'orders',
    foreignKey: 'inventoryId'
});
