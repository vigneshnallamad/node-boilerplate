import { Model, DataTypes } from 'sequelize';
import sequelize from './_index';
import { Orders } from './orders.model';
import { Inventory } from './inventory.model';

export class OrderInventory extends Model {}

OrderInventory.init(
    {
        id: {
            type: DataTypes.INTEGER,
            allowNull: false,
            primaryKey: true,
            autoIncrement: true,
            field: 'id'
        },
        orderId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'order_id',
            references: {
                model: Orders,
                key: 'id'
            }
        },
        inventoryId: {
            type: DataTypes.INTEGER,
            allowNull: false,
            field: 'inventory_id',
            references: {
                model: Inventory,
                key: 'id'
            }
        }
    },
    {
        sequelize,
        modelName: 'OrderInventory',
        tableName: 'order_inventory',
        timestamps: false
    }
);
