'use strict';

import { getMetrics } from '../controllers/metrics.server.controller';

module.exports = app => {
    app.route('/metrics').get(getMetrics);
};
