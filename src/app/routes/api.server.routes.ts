'use strict';

import { getOrder, createOrder } from '../controllers/orders.api.controller';

// import {
//     resolveToken,
//     resolveSecret
// } from '../controllers/auth.server.controller';

module.exports = function(app) {
    // TODO: Integrate login and verify JWT for authentication
    // app.route('/orders').get(resolveToken, resolveSecret, getOrder);
    // app.route('/orders/:id').get(resolveToken, resolveSecret, getOrder);
    // app.route('/orders').put(resolveToken, resolveSecret, createOrder);
    // app.route('/orders/:id').post(resolveToken, resolveSecret, createOrder);

    app.route('/orders').get(getOrder);
    app.route('/orders/:id').get(getOrder);
    app.route('/orders').put(createOrder);
    app.route('/orders/:id').post(createOrder);
};
