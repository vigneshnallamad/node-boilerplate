import { Inventory } from '../models/inventory.model';

export const checkAndAllocateInventory = async (item, transaction = null) => {
    // here we are making use of optimistic locking property of sequelize to remove race condition
    /*
        Optimistic locking allows concurrent access to model records for edits
        and prevents conflicts from overwriting data. It does this by checking
        whether another process has made changes to a record since it was read
        and throws an OptimisticLockError when a conflict is detected.
        https://sequelize.org/v5/manual/models-definition.html#optimistic-locking
    */
    let inventory = await Inventory.findOne({
        where: { id: item.id }
    });
    // Check if the inventory has items remaining
    if (
        !inventory ||
        inventory.remaining === 0 ||
        inventory.remaining < item.quantity
    ) {
        throw new Error(`${inventory.name} not available`);
    }
    inventory.remaining -= item.quantity;
    return inventory.save({ transaction });
};
