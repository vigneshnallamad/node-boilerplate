import { Orders } from '../models/orders.model';
import { Account } from '../models/account.model';
import { Inventory } from '../models/inventory.model';
import sequelize from '../models/_index';
import { log } from '../utils/error.utils';
import { createOrderInventory } from './orderInventory.service';

export const getOrder = (orderId = null) => {
    let include = [
        {
            model: Account,
            as: 'account'
        },
        {
            model: Inventory,
            as: 'inventory'
        }
    ];
    if (orderId) {
        return Orders.findOne({
            where: { id: orderId },
            include
        });
    }
    return Orders.findAll({
        include
    });
};

export const createOrder = async orderData => {
    let transaction, order, orderInventory;

    try {
        // break out if no items
        if (
            !orderData.accountId ||
            !(await Account.findOne({ where: { id: orderData.accountId } }))
        ) {
            throw new Error('Account ID is required');
        }
        // get transaction
        transaction = await sequelize.transaction();
        // create order
        order = await Orders.create(
            {
                accountId: orderData.accountId
            },
            {
                transaction
            }
        );
        // create order inventory entry and reduce the remaining count from inventory
        orderInventory = await createOrderInventory(
            order.id,
            orderData.items,
            transaction
        );
        if (orderInventory) {
            // commit
            await transaction.commit();
        }
    } catch (err) {
        log('error', err.message);
        // Rollback transaction only if the transaction object is defined
        if (transaction) {
            await transaction.rollback();
        }
        return {
            status: 422,
            success: false,
            error: err.message
        };
    }
    return {
        status: 200,
        success: true,
        data: {
            order,
            orderInventory
        }
    };
};
