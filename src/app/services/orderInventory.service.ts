import { OrderInventory } from '../models/orderInventory.model';
import { checkAndAllocateInventory } from './inventory.service';

export const createOrderInventory = async (
    orderId,
    items,
    transaction = null
) => {
    // break out if no items
    if (!items.length) {
        throw new Error('No items in cart');
    }
    let orderInventory = [];
    for (let i = 0; i < items.length; i++) {
        // for each of the items sent, we will check if the item is remaining in inventory
        let inventory = await checkAndAllocateInventory(items[i], transaction);
        // create an array of order inventory to create at once
        orderInventory.push({
            orderId,
            inventoryId: inventory.id
        });
    }
    return OrderInventory.bulkCreate(orderInventory, {
        transaction
    });
};
