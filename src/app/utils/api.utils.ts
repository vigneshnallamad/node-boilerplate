import axios from 'axios';
import { log } from './error.utils';

/**
 * Make an API Call by passing the payload
 * @param url
 * @param payload
 * @param auth
 * @param method
 */
export const apiCall = async function(apiOptions) {
    try {
        apiOptions.headers = apiOptions.headers || {
            Accept: 'application/json;charset=UTF-8'
        };

        apiOptions.responseType = apiOptions.responseType || 'json';

        apiOptions.method = apiOptions.method || 'GET';

        log('info', {
            msg: 'Routing',
            url: apiOptions.url
        });

        let response = await axios(apiOptions);

        return {
            err: null,
            response: response.data
        };
    } catch (err) {
        log('error', {
            message: 'Error in routing url',
            url: apiOptions.url,
            err: err
        });

        return {
            err: err,
            response: null
        };
    }
};
