import {
    getOrder as getOrderService,
    createOrder as createOrderService
} from '../services/order.service';
/*
    Uncomment these lines to run db migration
    import sequelize from '../models/_index';
    import { OrderInventory } from '../models/orderInventory.model';
 */

export const getOrder = async (req, res) => {
    /*
    // Uncomment these lines to run db migration
    await sequelize.sync();
    await OrderInventory.sync();
    // Sample insert for account
    INSERT INTO `account`
    (`id`, `name`, `mobile`, `address`, `createdAt`, `updatedAt`) VALUES
    (1, 'test user', '9538809413', 'address of user', '2020-06-06 20:00:00', '2020-06-06 20:00:00');
    // Sample insert for inventory
    INSERT INTO `inventory`
    (`id`, `name`, `count`, `remaining`, `createdAt`, `updatedAt`, `version`) VALUES
    (2, 'pen', 10, 9, '2020-06-06 20:00:00', '2020-06-06 20:07:54', 1);
     */
    let orderId = req.params.id || null;
    let orders = (await getOrderService(orderId)) || [];
    return res.status(200).json({
        success: true,
        orders
    });
};

export const createOrder = async (req, res) => {
    let orderData = req.body;
    let order = await createOrderService(orderData);
    return res.status(order.status).json(order);
};
